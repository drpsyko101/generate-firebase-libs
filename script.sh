#!/bin/bash

set -o pipefail

exit_on_error() {
  echo -e "\e[31mERROR: $1"
  echo "Exiting..."
  exit 1
}

export CC=/usr/bin/clang
export CXX=/usr/bin/clang++

sed -i '/-Wno-maybe-uninitialized/d' ../CMakeLists.txt
echo "Configuring Firebase CMake..."
cmake \
  -DCMAKE_CXX_FLAGS="$CMAKE_CXX_FLAGS -std=c++17 -stdlib=libc++" \
  -DCMAKE_EXE_LINKER_FLAGS="$CMAKE_EXE_LINKER_FLAGS -stdlib=libc++ -lc++abi" \
  -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
  -DCMAKE_BUILD_TYPE=Debug \
  -G "Ninja" .. || exit_on_error "Failed to generate Makefile"

echo "Generate Firebase libraries..."
ninja || exit_on_error "Failed to generate app"

if [[ -d "/output" ]]; then
  printf "Copying firebase linux binaries to output folder..."
  find . -type f -regex ".*libfirebase_.*\.a" -exec cp {} /output \; && echo "done."
  printf "Copying required external libraries..."
  cp external/src/flatbuffers-build/libflatbuffers.a /output && echo "done."
else
  echo -e "e\[33mWARNING: Output folder not found. Skipping copy."
fi
