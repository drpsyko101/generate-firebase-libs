FROM ubuntu AS base
RUN apt update && \
  apt install -y build-essential git && \
  apt clean

FROM base AS openssl_build
RUN git clone https://github.com/openssl/openssl.git
WORKDIR /openssl
RUN ./Configure
RUN make

FROM base AS firebase_build
RUN git clone https://github.com/firebase/firebase-cpp-sdk.git

FROM base AS output
RUN apt install -y cmake clang libc++-dev libc++abi-dev python3 python-is-python3 python3-absl python3-venv python3-distutils-extra ninja-build libsecret-1-dev && \
  apt clean
COPY script.sh /
COPY --from=openssl_build --chown=ubuntu:ubuntu /openssl /openssl
COPY --from=firebase_build --chown=ubuntu:ubuntu /firebase-cpp-sdk /firebase-cpp-sdk
USER ubuntu
WORKDIR /firebase-cpp-sdk/desktop_build
ENV OPENSSL_ROOT_DIR=/openssl
CMD [ "/script.sh" ]
