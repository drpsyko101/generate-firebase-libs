# Generate Firebase Libs

Create Firebase Linux library with position independent code using Clang

## Getting started

Create the docker image by using this command:

```bash
docker build -t <image_name> .
```

Run said image with desired folder mounted as `/output`:

```bash
docker run -it --rm -v $(pwd)/output:/output <image_name>
```

_`Ninja` is used to speed up the compile time._
